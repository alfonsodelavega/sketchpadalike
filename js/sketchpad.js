/*global $*/
/*global document*/

var width = 480;
var rows = 24;
var cols = 24;

function reDrawTiles(rows, cols) {
    $(".row").remove();
    for (var i = 0; i < rows; i += 1) {
        $("#tiles").append('<div class="row"></div>');
    }
    for (var j = 0; j < cols; j += 1) {
        $(".row").append('<div class="tile"></div>');
    }
    var val = width / rows;
    $(".tile").height(val);
    $(".tile").width(val);
}

$(document).ready(function () {
    reDrawTiles(rows, cols);
    document.getElementById("eraser").addEventListener("click", function () {
        $(".selected").removeClass("selected");
    });
    document.getElementById("reset").addEventListener("click", function () {
        var bottomLimit = 2,
            topLimit = 48;
        var size = prompt("Introduce the new dimension (Min: " + bottomLimit +
            ", Max: " + topLimit + ")");
        if (size !== null && size !== false && !isNaN(size) &&
            size >= bottomLimit && size <= topLimit) {
            reDrawTiles(size, size);
        }
    });
});

$(document).on("mouseover", ".tile", function () {
    $(this).addClass("selected");
});
